<?php 
session_start();
include('layout/config.php');

    ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="DynamicLayers">

    <title>Blog</title>

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

    <!-- Font Awesome Icons CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Themify Icons CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- Elegant Font Icons CSS -->
    <link rel="stylesheet" href="css/elegant-font-icons.css">
    <!-- Elegant Line Icons CSS -->
    <link rel="stylesheet" href="css/elegant-line-icons.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Venobox CSS -->
    <link rel="stylesheet" href="css/venobox/venobox.css">
    <!-- OWL-Carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <!-- Slick Nav CSS -->
    <link rel="stylesheet" href="css/slicknav.min.css">
    <!-- Css Animation CSS -->
    <link rel="stylesheet" href="css/css-animation.min.css">
    <!-- Nivo Slider CSS -->
    <link rel="stylesheet" href="css/nivo-slider.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
    <!-- <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div> -->
    <!-- Preloader -->

    <?php include('layout/header.php'); ?>

    <div class="header-height"></div>

    <!-- <div class="pager-header">
        <div class="container">
            <div class="page-content">
                <h2>Blog Grid</h2>
                <p>Help today because tomorrow you may be the one who <br>needs more helping!</p>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">Blog</li>
                </ol>
            </div>
        </div>
    </div> -->
    <!-- /Page Header -->

    <section class="blog-section bg-grey padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 xs-padding">
                    <div class="blog-items grid-list row">
                        <?php 
     if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        $no_of_records_per_page = 8;
        $offset = ($pageno-1) * $no_of_records_per_page;


        $total_pages_sql = "SELECT COUNT(*) FROM tblposts";
        $result = mysqli_query($con,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);


$query=mysqli_query($con,"select tblposts.id as pid,tblposts.PostTitle as posttitle,tblposts.PostImage,tblcategory.CategoryName as category,tblcategory.id as cid,tblsubcategory.Subcategory as subcategory,tblposts.PostDetails as postdetails,tblposts.PostingDate as postingdate,tblposts.PostUrl as url from tblposts left join tblcategory on tblcategory.id=tblposts.CategoryId left join  tblsubcategory on  tblsubcategory.SubCategoryId=tblposts.SubCategoryId where tblposts.Is_Active=1 order by tblposts.id desc  LIMIT $offset, $no_of_records_per_page");
while ($row=mysqli_fetch_array($query)) {
?>
                        <div class="col-md-4 padding-15">
                            <div class="blog-post">
                                <img src="admin/postimages/<?php echo htmlentities($row['PostImage']);?>"
                                    alt="blog post">
                                <div class="blog-content">
                                    <span class="date"><i class="fa fa-clock-o"></i>
                                        <?php echo htmlentities($row['postingdate']);?></span>
                                    <h3><a href="blog-single.php?nid=<?php echo htmlentities($row['pid'])?>"><?php echo htmlentities($row['posttitle']);?></a></h3>
                                    <!-- <p>The secret to happiness lies in helping others. Never underestimate the
                                        difference </p> -->
                                    <a href="blog-single.php?nid=<?php echo htmlentities($row['pid'])?>" class="post-meta">Read More</a>
                                </div>
                            </div>
                        </div><!-- Post 1 -->
                        <?php } ?>
                    </div>
                    <!-- <ul class="pagination_wrap align-center mt-30">
                        <li><a href="#"><i class="ti-arrow-left"></i></a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#" class="active">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#"><i class="ti-arrow-right"></i></a></li>
                    </ul> -->
                    <ul class="pagination_wrap align-center mt-30">
                        <li class="page-item"><a href="?pageno=1" class="page-link">1</a></li>
                        <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?> page-item">
                            <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>"
                                class="page-link">
                                prev</a>
                        </li>
                        <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?> page-item">
                            <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?> "
                                class="page-link">next</a>
                        </li>
                        <li class="page-item"><a href="?pageno=<?php echo $total_pages; ?>"
                                class="page-link"><?php echo $total_pages; ?></a>
                        </li>
                    </ul>

                    <!-- Pagination -->
                </div><!-- Blog Posts -->
            </div>
        </div>
    </section><!-- /Blog Section -->

    <!-- <section class="widget-section padding">
        <div class="container">
            <div class="widget-wrap row">
                <div class="col-md-4 xs-padding">
                    <div class="widget-content">
                        <img src="img/logo-light.png" alt="logo">
                        <p>The secret to happiness lies in helping others. Never underestimate the difference YOU can
                            make in the lives of the poor</p>
                        <ul class="social-icon">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 xs-padding">
                    <div class="widget-content">
                        <h3>Recent Campaigns</h3>
                        <ul class="widget-link">
                            <li><a href="#">First charity activity of this summer. <span>-1 Year Ago</span></a></li>
                            <li><a href="#">Big charity: build school for poor children. <span>-2 Year Ago</span></a>
                            </li>
                            <li><a href="#">Clean-water system for rural poor. <span>-2 Year Ago</span></a></li>
                            <li><a href="#">Nepal earthqueak donation campaigns. <span>-3 Year Ago</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 xs-padding">
                    <div class="widget-content">
                        <h3>Charitify Location</h3>
                        <ul class="address">
                            <li><i class="ti-email"></i> Info@YourDomain.com</li>
                            <li><i class="ti-mobile"></i> +(333) 052 39876</li>
                            <li><i class="ti-world"></i> Www.YourWebsite.com</li>
                            <li><i class="ti-location-pin"></i> 60 Grand Avenue. Central New Road 0708, USA</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    
    <!-- ./Widget Section -->

    <!-- <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 sm-padding">
                    <div class="copyright">&copy; 2021 Charitify Powered by DynamicLayers</div>
                </div>
                <div class="col-md-6 sm-padding">
                    <ul class="footer-social">
                        <li><a href="#">Orders</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Report Problem</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer> -->

    <?php include('layout/footer.php');?>
    
    <!-- /Footer Section -->

    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

    <!-- jQuery Lib -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/vendor/bootstrap.min.js"></script>
    <!-- Tether JS -->
    <script src="js/vendor/tether.min.js"></script>
    <!-- Imagesloaded JS -->
    <script src="js/vendor/imagesloaded.pkgd.min.js"></script>
    <!-- OWL-Carousel JS -->
    <script src="js/vendor/owl.carousel.min.js"></script>
    <!-- isotope JS -->
    <script src="js/vendor/jquery.isotope.v3.0.2.js"></script>
    <!-- Smooth Scroll JS -->
    <script src="js/vendor/smooth-scroll.min.js"></script>
    <!-- venobox JS -->
    <script src="js/vendor/venobox.min.js"></script>
    <!-- ajaxchimp JS -->
    <script src="js/vendor/jquery.ajaxchimp.min.js"></script>
    <!-- Counterup JS -->
    <script src="js/vendor/jquery.counterup.min.js"></script>
    <!-- waypoints js -->
    <script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>
    <!-- Slick Nav JS -->
    <script src="js/vendor/jquery.slicknav.min.js"></script>
    <!-- Nivo Slider JS -->
    <script src="js/vendor/jquery.nivo.slider.pack.js"></script>
    <!-- Letter Animation JS -->
    <script src="js/vendor/letteranimation.min.js"></script>
    <!-- Wow JS -->
    <script src="js/vendor/wow.min.js"></script>
    <!-- Contact JS -->
    <script src="js/contact.js"></script>
    <!-- Main JS -->
    <script src="js/main.js"></script>

</body>

<!-- Mirrored from html.dynamiclayers.net/dl/charitify/blog-grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Oct 2022 06:55:26 GMT -->

</html>