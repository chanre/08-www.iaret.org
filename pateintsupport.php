<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>PATIENT SUPPORT</h2>
<small>Direct and Indirect patient care for the needy and non-affordable</small><br>
<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/psupport.jpeg" alt="about image">
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2>Free Clinic</h2>
<p>
	As we all know there are patients who cannot access the best of care for Arthritis and Immunological diseases due to financial constraints. One of the prime objectives of this trust is to have direct and indirect care of the needy and non-affordable by free/subsidized treatment supplemented with free medicines.
  </p>
<p>At CRICR free clinic is organized for the benefit of the needy and non-affordable patients and available free medicines are dispensed from the trust drug bank.</p>
<p>IARE Trust tries to help the patients who find it difficult to afford proper diagnosis and management of their diseases in the following ways: </p>
<p></p>
<ul class="check-list">
<li><i class="fa fa-check"></i>Evaluate their need for support through counselling by Medico-Social Worker through a standardized preformed checklist. </li>
<li><i class="fa fa-check"></i>Trust recommends and extends suitable support to these patients. </li>
<li><i class="fa fa-check"></i>Free consultation / subsidized diagnostic tests, hospitalization facilities, etc. from ChanRe Rheumatology & Immunology Center & Research / ChanRe Diagnostic Laboratory.</li>
<!-- <li><i class="fa fa-check"></i><b>Drug bank</b> Disbursal of  drugs free of cost to the non-affordable patients from the trust drug bank as per the prescriptions of the doctors in CRICR. </li> -->

</ul>
<p>More than 100 patients are already supported from the trust. Many of these patients require regular followup visits which is also covered under this scheme. The trust has extended support for a total of 1200 followup visits for all of these patients. </p>

<ul class="check-list">
<li><i class="fa fa-check"></i>1) Contacting other social service organizations for direct additional support to the needy patients from them.  </li>
<li><i class="fa fa-check"></i>2) Contacting donors who would like to support individual / cluster of patients directly. </li>
</ul>

</div>
</div>
</div>
</div>
</section>



<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>