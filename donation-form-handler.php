<?php 
$errors = '';
$myemail = 'webdesigner@chanrejournals.com';

if(empty($_POST['amount'])  || 
	empty($_POST['gift'])  || 
	empty($_POST['source'])  || 
	empty($_POST['fname'])  || 
	empty($_POST['lname'])  || 
	empty($_POST['address'])  || 
	empty($_POST['city'])  || 
	empty($_POST['state'])  || 
	empty($_POST['country'])  || 
   empty($_POST['email']) || 
   empty($_POST['number']) || 
   empty($_POST['tfirstName']) || 
   empty($_POST['tlastName']) || 
   empty($_POST['type']) || 
   empty($_POST['message'])||
   empty($_POST['mode'])||
   empty($_POST['ref_No'])||
   empty($_POST['date'])||
  empty($_POST['bank'])

)
{
    $errors .= "\n Error: all fields are required";
}
	
$amount = $_POST['amount']; 
$gift = $_POST['gift']; 
$source = $_POST['source']; 
$fname = $_POST['fname']; 
$lname = $_POST['lname']; 
$address = $_POST['address']; 
$city = $_POST['city']; 
$state = $_POST['state']; 
$country = $_POST['country']; 
$email_address = $_POST['email']; 
$phone_number=$_POST['number']; 
$tfirstName=$_POST['tfirstName']; 
$tlastName=$_POST['tlastName']; 
$type=$_POST['type']; 
$message = $_POST['message']; 
$mode = $_POST['mode']; 
$ref_No = $_POST['ref_No']; 
$date = $_POST['date']; 
$bank = $_POST['bank']; 


if (!preg_match(
"/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", 
$email_address))
{
    $errors .= "\n Error: Invalid email address";
}

if( empty($errors))
{
	$to = $myemail; 

	$email_subject = "Contact form submission: $name";
	$email_body = "You have received a new message. ".
	" Here are the details:\n Amount: $amount\n Gift Type: $gift \n Source of information: $source \n First Name: $fname\n Last Name: $lname \n Address: $address \n City: $city \n State: $state \n Country: $country \n Email: $email_address\n Phone:$phone_number \n--TRIBUTE INFORMATION--\n First Name: $tfirstName \n Last Name: $tlastName \nType of tribute: $type \n Message:\n $message \n --PAYMENT DETAILS--\n Payment mode:$mode\n Reference:$ref_No\n Date:$date\n Bank Name:$bank\n ";
	
	$headers = "From: $email_address\n"; 
	$headers .= "Reply-To: $email_address";
	
	mail($to,$email_subject,$email_body,$headers);
	//redirect to the 'thank you' page
	header('Location: contact-form-thank-you.html');
} 
?>
<!DOCTYPE HTML> 
<html>
<head>
	<title>Contact form handler</title>
</head>

<body>
<!-- This page is displayed only if there is some error -->
<?php
echo nl2br($errors);
?>


</body>
</html>