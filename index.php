<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="DynamicLayers">
    <title>Immunology & arithritis Research & Eduaction Trust</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/elegant-font-icons.css">
    <link rel="stylesheet" href="css/elegant-line-icons.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/venobox/venobox.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/slicknav.min.css">
    <link rel="stylesheet" href="css/css-animation.min.css">
    <link rel="stylesheet" href="css/nivo-slider.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
    <!------------ POPUP ------------->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
    <style type="text/css">
        .bh{
            color: red;
            font-weight: 500;
        }
        
    .card {
        height: 400px;
        border-radius: 10px;
        background: #badaf2;
        box-shadow: 5px 5px 10px #9cb7cb,
            -5px -5px 10px #d8fdff;
    }
    #overlay {
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #000;
        filter: alpha(opacity=70);
        -moz-opacity: 0.7;
        -khtml-opacity: 0.7;
        opacity: 0.7;
        z-index: 100;
        display: none;
    }
    .cnt223 a {
        text-decoration: none;
    }
    .popup {
        width: 100%;
        margin-top: -50px;
        display: none;
        position: fixed;
        z-index: 101;
    }
    .cnt223 {
        min-width: 600px;
        width: 600px;
        min-height: 150px;
        margin: 100px auto;
        background: #f3f3f3;
        position: relative;
        z-index: 103;
        padding: 15px 35px;
        border-radius: 5px;
        box-shadow: 0 2px 5px #000;
    }
    @media only screen and (max-width: 600px) {
        .cnt223 {
            min-width: auto;
            width: auto;
            min-height: 150px;
            margin: 100px auto;
            background: #f3f3f3;
            position: relative;
            z-index: 103;
            padding: 15px 35px;
            border-radius: 5px;
            box-shadow: 0 2px 5px #000;
        }
    }
    .cnt223 p {
        clear: both;
        color: #555555;
        /* text-align: justify; */
        font-size: 20px;
        font-family: sans-serif;
    }
    .cnt223 p a {
        color: #d91900;
        font-weight: bold;
    }
    .cnt223 .x {
        float: right;
        height: 35px;
        left: 22px;
        position: relative;
        top: -25px;
        width: 34px;
    }
    .cnt223 .x:hover {
        cursor: pointer;
    }
    .button,
    .mybtn a {
        background-color: #004A7F;
        -webkit-border-radius: 10px;
        border-radius: 10px;
        border: none;
        color: #FFFFFF;
        cursor: pointer;
        display: inline-block;
        font-family: Arial;
        font-size: 20px;
        padding: 5px 10px;
        text-align: center;
        text-decoration: none;
        -webkit-animation: glowing 1500ms infinite;
        -moz-animation: glowing 1500ms infinite;
        -o-animation: glowing 1500ms infinite;
        animation: glowing 1500ms infinite;
    }
    @-webkit-keyframes glowing {
        0% {
            background-color: #B20000;
            -webkit-box-shadow: 0 0 3px #B20000;
        }

        50% {
            background-color: #FF0000;
            -webkit-box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            -webkit-box-shadow: 0 0 3px #B20000;
        }
    }
    @-moz-keyframes glowing {
        0% {
            background-color: #B20000;
            -moz-box-shadow: 0 0 3px #B20000;
        }

        50% {
            background-color: #FF0000;
            -moz-box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            -moz-box-shadow: 0 0 3px #B20000;
        }
    }
    @-o-keyframes glowing {
        0% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }
        50% {
            background-color: #FF0000;
            box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }
    }
    @keyframes glowing {
        0% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }

        50% {
            background-color: #FF0000;
            box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }
    }
    .poster img {
        max-width: 100%;
        /*margin-top: -100px;*/
    }
    </style>
    <script type='text/javascript'>
    $(function() {
        var overlay = $('<div id="overlay"></div>');
        overlay.show();
        overlay.appendTo(document.body);
        $('.popup').show();
        $('.close').click(function() {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });
        $('.x').click(function() {
            $('.popup').hide();
            overlay.appendTo(document.body).remove();
            return false;
        });
    });
    </script>
    <!------------ POPUP ------------->
	
	
    <div class='popup'>
        <div class='cnt223'>
            <p><a href='#' class="close button">X</a></p>
            <div class="poster">
                <img src="https://chanrericr.com/img/lupuspopup.JPG">
                <div class="mybtn">
                    <a href="lupusday.php" class="btn">View details</a>
                </div>

            </div>
        </div>
    </div>
    <!------------ POPUP ------------->
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>
    <?php include('layout/header.php'); ?>
    <div class="header-height"></div>
    <section class="slider-section">
        <div class="slider-wrapper">
            <div id="main-slider" class="nivoSlider">
                <!-- <img src="images/lupus.jpg" alt="" title="#slider-caption-1" /> -->
                <img src="img/slider-2.jpg" alt="" title="#slider-caption-2" />
                <img src="img/slider-3.jpg" alt="" title="#slider-caption-3" />
            </div>
            <div id="slider-caption-1" class="nivo-html-caption slider-caption">
                <div class="display-table">
                    <div class="table-cell">
                        <div class="container">
                            <div class="slider-text">
                                <!-- <h1 class="wow cssanimation leFadeInRight sequence">Better Life for People</h1>
<p class="wow cssanimation fadeInTop" data-wow-delay="1s">Help today because tomorrow you may be the one who needs helping! <br>Forget what you can get and see what you can give.</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="slider-caption-2" class="nivo-html-caption slider-caption">
                <div class="display-table">
                    <div class="table-cell">
                        <div class="container">
                            <div class="slider-text">
                                <h1 class="wow cssanimation fadeInTop" data-wow-delay="1s" data-wow-duration="800ms">
                                    Together we <br>can make a Difference</h1>
                                <p class="wow cssanimation fadeInBottom" data-wow-delay="1s">Help today because tomorrow
                                    you may be the one who needs helping! <br>Forget what you can get and see what you
                                    can give.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="slider-caption-3" class="nivo-html-caption slider-caption">
                <div class="display-table">
                    <div class="table-cell">
                        <div class="container">
                            <div class="slider-text">

                                <h1 class="wow cssanimation lePushReleaseFrom sequence" data-wow-delay="1s">Give a
                                    little. Change a lot.</h1>
                                <p class="wow cssanimation fadeInTop" data-wow-delay="1s">Help today because tomorrow
                                    you may be the one who needs helping! <br>Forget what you can get and see what you
                                    can give.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="promo-section bd-bottom">
        <div class="promo-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 xs-padding">
                        <div class="promo-content">
                            <h3>Trust Details</h3>
                            <ul>
                                <li><span class="bh"> Trust Registration No.:</span> 262/99-2000, dated 25.02.2000</li>
                                <li><span class="bh">12A Reg. No.:</span> AAATI3936PE20221 (AY 2022-23 to AY 2025-26)</li>
                                <li><span class="bh">80G Reg. No.:</span> AAATI3936PF20221 (27/09/2022 to AY 2025-26)</li>
                                <li><span class="bh">NITI Aayog:</span> Darpan Unique ID: KA/2021/0277784, dated 22.03.2021</li>
                            </ul>
                            <a href="overview.php" class="btn btn-warning btn-sm">Read More</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 xs-padding">
                        <div class="promo-content">
                            <h3>Our Mission</h3>
                            <p>
                                To increase the awareness of Specialized Medical Services & to create knowledge base for
                                future generations.

                                To share knowledge and capability for the benefit of the needy & to be empathetic to
                                community by service.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 xs-padding">
                        <div class="promo-content">
                            <h3>Make Donation</h3>
                            <p>100% trusted organization.</p>
                            <a href="/donate.php">Click Here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial-section bd-bottom padding">
        <div class="container">
            <div class="section-heading text-center mb-40">
                <h2>Our Team</h2>
                <span class="heading-border"></span>
                <p></p>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="testi-footer">
                        <img src="img/team/ren.jpg" alt="profile">
                        <h4>Dr. Renuka. C DNB, DCP<span>Trustee Chairperson</span></h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="testi-footer">
                        <img src="img/team/chan.jpg" alt="profile">
                        <h4>Dr. Chandrashekara. S MD, DNB, DM<span>Managing Trustee</span></h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="testi-footer">
                        <img src="img/team/dm.png" alt="profile">
                        <h4>Dr. Dharmanand. B. G MD, DM<span>Trustee</span></h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="testi-footer">
                        <img src="img/team/ragha.png" alt="profile">
                        <h4>Dr. Raghavendra Rao, MNYS<span>TRustee</span></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial-section bd-bottom padding">
        <div class="container">
            <div class="section-heading text-center mb-40">
                <h2>Glimpse of our work</h2>
                <span class="heading-border"></span>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Inauguration ChanRe Update 2014</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th2.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Award of Excellence ChanRe Update 2014</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th3.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Award of Merit ChanRe Update 2014</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th4.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Scientific Gathering ChanRe Update 2014</h5>
                        </div>
                    </div>
                </div>



            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th5.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Release of Souvenir ChanRe Update 2014</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th6.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Primary Physician Update Knowledge Sharing</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th7.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Arthritis camp Community Service</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th8.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Research Support</h5>
                        </div>
                    </div>
                </div>

            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <img class="card-img-top" src="img/work/th9.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Free Clinics Patient Support</h5>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 sm-padding">
                    <div class="copyright">&copy; IARET 2020</div>
                </div>
                <div class="col-md-6 sm-padding">

                </div>
            </div>
        </div>
    </footer>
    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/tether.min.js"></script>
    <script src="js/vendor/imagesloaded.pkgd.min.js"></script>
    <script src="js/vendor/owl.carousel.min.js"></script>
    <script src="js/vendor/jquery.isotope.v3.0.2.js"></script>
    <script src="js/vendor/smooth-scroll.min.js"></script>
    <script src="js/vendor/venobox.min.js"></script>
    <script src="js/vendor/jquery.ajaxchimp.min.js"></script>
    <script src="js/vendor/jquery.counterup.min.js"></script>
    <script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>
    <script src="js/vendor/jquery.slicknav.min.js"></script>
    <script src="js/vendor/jquery.nivo.slider.pack.js"></script>
    <script src="js/vendor/letteranimation.min.js"></script>
    <script src="js/vendor/wow.min.js"></script>
    <script src="js/contact.js"></script>
    <script src="js/main.js"></script>
</body>


</html>