<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>ANNUAL SCIENTIFIC UPDATES</h2>
<small></small><br>
<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<!-- <img src="img/team/dm.png" alt="about image"> -->
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2></h2>
<p>
	As a major thrust in the education activity, IARET has been involved in the organization and conducting of the annual scientific update – ChanRe Update, continuously from the year 2003. 
  </p>
<p>This full day scientific update programme consists of deliberations on a particular area of importance in Immunology / Rheumatology with a stress on the newer development each year.</p>
<p>The deliberations in this CME programme is concentrated on the basic scientific aspects as well as the diagnostic and medical management areas of the topic of the year </p>
<p>Deliberations are conducted by a panel of eminent specialists in the field of interest from different parts of India with specialist from CRICR and CDL.</p>

<p>In addition to the above, the event also involves the Post MD Immunology & Rheumatology fellowship students and junior faculty from CRICR and CDL, involving in case presentations, quiz programmes, etc.  </p>
<p>The audience for all these programmes consists of invitees and registered delegates from among the primary physicians and post graduate medical students. </p>
<p>These events also focus the continuing developments happening in the field of Immunology & Rheumatology and showcasing of developments in both CRICR and CDL.</p>
<p>Over the period, involvement of practicing physicians and postgraduates has increased creating a lot of interest in these deliberations and the scientific and medical community in Bangalore await for this event ChanRe Update in the month of December.</p>
</div>
</div>
</div>
</div>
</section>



<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>