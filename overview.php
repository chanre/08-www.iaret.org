<!doctype html>

<html class="no-js" lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="DynamicLayers">
    <title>Immunology & arithritis Research & Eduaction Trust</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

    <link rel="stylesheet" href="css/font-awesome.min.css">

    <link rel="stylesheet" href="css/themify-icons.css">

    <link rel="stylesheet" href="css/elegant-font-icons.css">

    <link rel="stylesheet" href="css/elegant-line-icons.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/venobox/venobox.css">

    <link rel="stylesheet" href="css/owl.carousel.css">

    <link rel="stylesheet" href="css/slicknav.min.css">

    <link rel="stylesheet" href="css/css-animation.min.css">

    <link rel="stylesheet" href="css/nivo-slider.css">

    <link rel="stylesheet" href="css/main.css">

    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
	<style>
		.iaretb img{
			width:100%;
		}
	</style>
</head>

<body>

    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>
    <?php include('layout/header.php'); ?>
    <div class="header-height"></div>
    <div class="pager-header">
        <div class="container">
            <div class="page-content">
                <h2>Overview</h2>
                <p> </p>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">Overview</li>
                </ol>
            </div>
        </div>
    </div>
    <section class="about-section bg-grey bd-bottom padding">
        <div class="container">
            <div class="row about-wrap">
                <div class="col-md-2"></div>
                <div class="col-md-8">
					<div class="iaretb">
						<img src="img/iaretb.jpg" alt="">
					</div>
                    
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row about-wrap">
                <div class="col-md-6 xs-padding">
                    <div class="about-image">
                        <img src="img/about.jpg" alt="about image">
                    </div>
                </div>
                <div class="col-md-6 xs-padding">
                    <div class="about-content">
                        <h2></h2>
                        <p>Arthritis is a chronic disabling disease affecting more than 15% of the population.
                            Immunological alteration is responsible for the disease. This disease produces a lot of
                            disability and handicap. With an early and appropriate intervention this disability in
                            preventable. Ignorance and lack of awareness about this disease is the major contributor to
                            the disability. </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="team-section bd-bottom padding">
        <div class="container">
            <div class="section-heading text-center mb-40">

                <p>As early as 128-200 AD, the great Physician Galen pointed out in his treatise “Now is not a
                    disgraceful state of affaire that a human who had an excellent constitution should need to be
                    carried about by other people owing to Gout. Is it not disgraceful that a person should, by reason
                    of that extraordinary thing Arthritis, be unable to use his hands and should need somebody else to
                    bring food to his mouth and to perform his toilet necessary for him. And even if one overlook the
                    disgraceful aspect of this disease, yet one cannot overlook the pain these people suffer. And the
                    cause for all this must be referred to dissipation or ignorance or both.”</p>

                <p>Contrary to the common belief that Arthritis is a disease of the aged people, it also affects younger
                    age groups also. It is not a rare disease. More than 15% of general population suffer from
                    Arthritis, with 1% of them suffering from a very severe form of this disease. As we are an ageing up
                    population, we have the increasing burden of caring for the elderly. In this elderly group more than
                    40% suffer from incapacitating joint pains. To mitigate the pain and disability of them we need to
                    rise-up and meet the challenge.</p>

                <p>The treatment modalities for Arthritis, especially the immunological Arthritis, Rheumatoid Arthritis
                    still needs further refinement. There is a felt need for further research in this field. There are
                    no funding and supporting bodies in the both Government and Non- Government sectors. There is a need
                    for such Organisation to support some research work in this field especially with reference to the
                    Indian context.</p>

                <p>
                    The Speciality services in the field of Arthritis and Immunology are not accessible to the poor and
                    needy. Also, these services are not available extensively. A lot of effort is required to have
                    trained personnel. An organization should support and encourage these personnel.
                </p>
                <p>
                    There is a need for improvement in the very outlook regarding the management of Arthritis. Changes
                    in attitude are required both from the professionals & the patients. In order to achieve these goals
                    a platform is required to conceptualize, formulate and put into practice through different
                    programmes.
                </p>
                <p>
                    The Immunology & Arthritis Research & Education Trust is created with the clear idea of catering to
                    the needs of non-affordable in getting specialist care in Immunological and Rheumatological
                    diseases, to promote research in Immunology and Rheumatology, to undertake awareness programmes both
                    for the primary physicians and general public as also to conduct free camps and clinics to identify
                    new cases and help them get proper therapy.
                </p>
            </div>
            <div class="team-wrapper row">
                <div class="col-lg-3 sm-padding">
                    <div class="team-wrap row">
                        <div class="col-md-3">
                            <div class="">


                            </div>
                        </div>



                    </div>
                </div>
                <div class="col-lg-12 sm-padding">
                    <div class="team-content">

                    </div>

                </div>
            </div>
        </div>
    </section>


    <?php include ('layout/footer.php'); ?>
    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script src="js/vendor/tether.min.js"></script>

    <script src="js/vendor/imagesloaded.pkgd.min.js"></script>

    <script src="js/vendor/owl.carousel.min.js"></script>

    <script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

    <script src="js/vendor/smooth-scroll.min.js"></script>

    <script src="js/vendor/venobox.min.js"></script>

    <script src="js/vendor/jquery.ajaxchimp.min.js"></script>

    <script src="js/vendor/jquery.counterup.min.js"></script>

    <script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

    <script src="js/vendor/jquery.slicknav.min.js"></script>

    <script src="js/vendor/jquery.nivo.slider.pack.js"></script>

    <script src="js/vendor/letteranimation.min.js"></script>

    <script src="js/vendor/wow.min.js"></script>

    <script src="js/contact.js"></script>

    <script src="js/main.js"></script>
</body>


</html>