<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	.about-image{
		padding: 50px 0px 0px 0px;
		height: 50%;
		width: 50%;
		margin-right: auto;
		margin-left: auto;
		
		
	}
	.about-image img{
border-radius: 50px;
background: linear-gradient(145deg, #e6e6e6, #ffffff);
box-shadow:  5px 5px 10px #666666, 
             -5px -5px 10px #ffffff;
	}
	.about-wrap{
		padding: 40px;
		border-radius: 50px;
background: linear-gradient(315deg, #e6e6e6, #ffffff);
box-shadow:  -5px -5px 10px #666666, 
             5px 5px 10px #ffffff;
	}
	.about-image img:hover{
border-radius: 50px;

width: 100%;
background: linear-gradient(145deg, #e6e6e6, #ffffff);
box-shadow:  5px 5px 10px #666666, 
             -5px -5px 10px #ffffff;
	}
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>IARET is proud to have a unique, talented, and passionate team</h2>
<span class="heading-border"></span>
</div>

<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/team/ren.jpg" alt="about image">
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2>TRUSTEE CHAIRPERSON</h2>
<h3>Dr. Renuka. P</h3>
<p>  DCP, DNB
Director & Pathologist, 
ChanRe Diagnostic Laboratory &
Director, CRICR
  </p>
<p>Dr. Renuka. P is a Pathologist of 20 years of experience and expertise. After completing her MBBS from B. J. Medical College, Pune, started her journey towards specialization in Pathology, by completing DCP in the year 1999 from St. John's Medical College, Bangalore and DNB Pathology in the year 2001. After working in St. John's medical college as a lecturer for three years, she established ChanRe Diagnostic Laboratory (CDL) in 2003. </p>
<p>Over the years, she was instrumental in bringing up CDL to the current status of a recognized referral medical diagnostic laboratory, with all the branches of diagnosis and a unique specialty unit for Immunodiagnosis and DNA Diagnostics. Under her leadership, CDL got the NABL accreditation in the year 2010. </p>
<p>She is a member of Indian Pathologists Association and Executive Committee member of Society of Inflammation Research (SIR).</p>
<p>She has several publications in national & international journals to her credit & she is instrumental in disseminating knowledge by organizing several Workshops in Immuno-diagnosis & other fields of Pathology. </p>
</div>
</div>
</div>
 <br>
<div class="row about-wrap">	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/team/chan.jpg" alt="about image">
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2>MANAGING TRUSTEE</h2>
<h3>Dr. Chandrashekara. S</h3>
<p>MD, DNB, DM
Medical Director, 
ChanRe Rheumatology & Immunology Center & Research 

  </p>
<p>Dr. Chandrashekara. S is a renowned rheumatologist and Immunologist with over 20 years of consultant grade experience and expertise. He received his MD from Mysore Medical college, Karnataka and DNB in General Medicine from New Delhi, India. He subsequently pursued DM in Clinical Immunology and Rheumatology from Sanjay Gandhi Postgraduate institute of Medical Sciences, Lucknow.</p>
<p>Dr. Chandrashekara started his career as Assistant Professor and Consultant Immunologist in M. S. Ramaiah Medical College, Bangalore and played a key role in establishing the Dept. of Immunology & Rheumatology in the institute. He got promoted to the designation of Professor in June 2006 for his pioneering and active research work in the field of Immunology & Rheumatology. In order to fulfil his vision, he established ChanRe Rheumatology & Immunology Center & Research, a specialized one-stop center providing care and treatment for diverse autoimmune diseases, in the year 2002. He has been awarded with ‘Sir. C. V. Raman Young Scientist State Award in the field of Medical Sciences’, by the Govt. of Karnataka for the year 2013. </p>
<p>He has multiple publications in international and national journals, papers in Conferences / seminars, more than 10 reference books in Rheumatology & several patients education books and he has to his credit many chapters written in reference books and several articles in newsletters. He has also conducted CMEs and training programs for general practitioners, physicians and orthopaedicians.</p>
<p>Currently he is the Vice President – Indian Rheumatology Association (IRA), President – IRA-Karnataka Chapter, General Secretary – Society of Inflammation Research and Executive Editor – ChanRe Journals. </p>
</div>
</div>

</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/team/dm.png" alt="about image">
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2>TRUSTEE</h2>
<h3>Dr. Dharmanand. B. G</h3>
<p>
	MD, DNB
Consultant Rheumatologist, Vikram Hospital, Bengaluru.
  </p>
<p>Dr. B. G. Dharmanand is one of the senior most Rheumatologists in the state of Karnataka. He did his undergraduate training in Madurai Medical College and MD Internal Medicine in Mysore Medical College. He finished his Super Speciality training (DM) in Rheumatology from the prestigious Madras Medical College. He started one of the first Rheumatology departments in Manipal Hospital, Bangalore in the year 1997 and served there as head of the Department till November 2013. Barring a brief period between 2002 and 2004 when he went to the United Kingdom for higher training. He was also one of the directors and consultant in  ChanRe Rheumatology & Immunology Centre & Research till December 2013.</p>
<p>A Rheumatology department in the Sakra World Hospital, Bengaluru was headed by                                                         Dr. Dharmanand from the year 2014. Currently he is one of the Consultant Rheumatologist in Vikram Hospital, Bengaluru  </p>
<p>Dr. B. G. Dharmanand is the President-Elect of Indian Rheumatology Association (IRA). He is the past president of IRA-Karnataka Chapter.</p>

</div>
</div>
</div>

<br>
<div class="row about-wrap">	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/team/ragha.png" alt="about image">
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2>TRUSTEE</h2>
<h3>Dr. Raghavendra Rao</h3>
<p>Director, Central Council for Research in Yoga and Naturopathy (CCRYN), 
Ministry of AYUSH, Govt. of India.
  </p>
<p>Dr. Raghavendra Rao is currently the Director, Central Council for Research in Yoga and Naturopathy (CCRYN), Ministry of AYUSH, Govt. of India. He was a Senior Scientist of the Clinical Research and Head CAM Program at Health Care Global Enterprises Ltd.                      Earlier to this, he was the senior scientist in the department of Oncology, HCG. Before he joined HCG, he was Asst. Professor of Yoga and Life Sciences in S-Vivekananda Yoga Anusandhana Samsthana University - SVYASA Yoga University after completing his Ph. D in Yoga and Life Sciences.</p>
<p>He has completed several research projects in rheumatoid arthritis, prediabetes, diabetes, CVD risk prevention adn the use of both yoga and naturopathy interventions. He has written many international research publications and book chapters on yoga and its application.</p>

</div>
</div>

</div>

</div>
</section>
<section class="team-section bd-bottom padding">
<div class="container">
<div class="section-heading mb-40">
<p><h3 class="text-center">Organizations in support</h3>
<hr>
The trust is happy to associate with the following organizations. Through the support of the following organizations, all its activities towards the needy and non-affordable patients and community services is extended. 
<br>
1.	<a href="https://chanrericr.com/">ChanRe Rheumatology & Immunology Center & Research (CRICR)</a>  

<br>
2.	<a href="https://www.chanrediagnostic.com/">ChanRe Diagnostic Laboratory (CDL)</a>
                 

</p>
</div>
<div class="team-wrapper row">
<div class="col-lg-3 sm-padding">
<div class="team-wrap row">
<div class="col-md-3">
<div class="">


</div>
</div>



</div>
</div>
<div class="col-lg-12 sm-padding">
<div class="team-content">

</div>

</div>
</div>
</div>
</section>


<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>