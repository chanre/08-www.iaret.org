<?php 

session_start();
include('config.php');

if(isset($_POST['submit']))
{
     $AdminUserName=$_POST['AdminUserName'];
     $AdminPassword=$_POST['AdminPassword'];
     $AdminEmailId=$_POST['AdminEmailId'];

	 $pid=$_POST['pid'];
	 $status=0;
	 $AdminPassword= password_hash($AdminPassword, PASSWORD_DEFAULT);
    $query=mysqli_query($con,"Insert into patientdiagram (AdminUserName,AdminEmailId,AdminPassword,Is_Active,pid) values('$AdminUserName','$AdminEmailId','$AdminPassword','$status','$pid')");

if($query){
$msg="Successfully Submitted..Wait for approval";
}
else
{
$msg="Couldn't process your request!!! Can you please try again..";
}
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
       body{
        background-color: #e7eaf7;
       }
       .slider {
  -webkit-appearance: none;
  width: 100%;
  height: 25px;
      background-image: linear-gradient(to right, #ffc107 , #dc3545);
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}



#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}

#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
	font-family:times;
}
img{
	
	width:98%;
}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
  

    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include('layout/header.php');?>

       <div class="container-fluid">
            <div class="row">
			<div class="col-md-4"></div>
                <div class="col-md-4">
                  
                    <br><br>
					<h6 style="color:red;font-size:18px;font-style:bold;background-color:yellow;text-align:center;"><?php echo "$msg"; ?></h6>
					<form method="post">
					<table align="center" border="5px">
			<tr>
				<td>Name</td>
				<td><input class="form-control" type="text" required="" name="AdminUserName" placeholder="Enter name" autocomplete="off"></td>
			</tr>
			<!--<tr>
				<td>Contact</td>
				<td><input class="form-control" type="text" required="" name="contact" placeholder="Enter contact" autocomplete="off"></td>
			</tr>
			-->
			<tr>
				<td>Patient id</td>
				<td><input class="form-control" type="text" required="" name="pid" placeholder="Enter Patient id.." autocomplete="off"></td>
			</tr>
			
			<tr>
				<td>Email</td>
				<td><input class="form-control" type="text" required="" name="AdminEmailId" placeholder="Enter Email" autocomplete="off"></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input class="form-control" type="password" name="AdminPassword" required="" placeholder="Password" autocomplete="off"></td>
			</tr>

		</table>
		<div style="width: 10%;margin-left: auto;margin-right: auto;margin-top: 30px;" align="center">
			<input type="submit" name="submit" value="submit" class="btn btn-primary">

		</div>
				</form>	<br>
				
                </div>
                
                <div class="col-md-2"></div>
            </div>
        </div>

        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="jquery.min.js"></script>
<script src="jquery.multiselect.js"></script>
<script>
$('#langOpt').multiselect({
    columns: 1,
    placeholder: 'Select'
});

$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Select',
    search: true
});

$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select where you had pain past week',
    search: true,
    selectAll: true
});
$('#langOpt4').multiselect({
    columns: 1,
    placeholder: 'Select where you had swelling past week',
    search: true,
    selectAll: true
});
$('#langOptgroup').multiselect({
    columns: 4,
    placeholder: 'Select',
    search: true,
    selectAll: true
});
</script>
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>
            <script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
</script>



</body>

</html>
