<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include ('layout/header.php'); ?>
<div class="header-height"></div>
<div class="pager-header">
<div class="container">
<div class="page-content">
<h2>Mission</h2>
<p> </p>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Home</a></li>
<li class="breadcrumb-item active">Mission</li>
</ol>
</div>
</div>
</div>

<section class="team-section bd-bottom padding">
<div class="container">
<div class="section-heading text-center mb-40">
<h2>Vision and Mission</h2>
<span class="heading-border"></span>
<p></p>
</div>
<div class="team-wrapper row">
<div class="col-lg-3 sm-padding">
<div class="team-wrap row">
<div class="col-md-3">
<div class="">


</div>
</div>



</div>
</div>
<div class="col-lg-12 sm-padding">
<div class="team-content">

<h3>Vission</h3>

<ul class="check-list">
<li><i class="fa fa-check"></i>To increase the awareness of Specialized Medical Services</li>
<li><i class="fa fa-check"></i>To create knowledge base for future generations</li>
<li><i class="fa fa-check"></i>To educate for better treatment.</li>
<li><i class="fa fa-check"></i>To train for better management.</li>
<li><i class="fa fa-check"></i>To recognize talent by award</li>
<li><i class="fa fa-check"></i>To share knowledge and capability for the benefit of the needy</li>
<li><i class="fa fa-check"></i>To be empathetic to community by service</li>
<li><i class="fa fa-check"></i>To join hand to serve the humanity.</li>

</ul>
</div>
<div class="team-content">

<h3>Mission</h3>

<ul class="check-list">
<li><i class="fa fa-check"></i>To create better awareness about arthritis conditions. </li>
<li><i class="fa fa-check"></i>To find and extend cost effective treatment to patients.</li>
<li><i class="fa fa-check"></i>Support the arthritis patients with proper diagnosis and free / subsidized treatment</li>
<li><i class="fa fa-check"></i>To train and educate the medical team including primary physician and paramedics in the field of immunological diseases</li>
<li><i class="fa fa-check"></i>To recognize latent talent in this field </li>

</ul>
</div>


</div>
</div>
</div>
</section>


<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>