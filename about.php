<!doctype html>

<html class="no-js" lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="DynamicLayers">
    <title>Immunology & arithritis Research & Eduaction Trust</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/elegant-font-icons.css">
    <link rel="stylesheet" href="css/elegant-line-icons.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/venobox/venobox.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/slicknav.min.css">
    <link rel="stylesheet" href="css/css-animation.min.css">
    <link rel="stylesheet" href="css/nivo-slider.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>
    <?php include ('layout/header.php'); ?>
    <div class="header-height"></div>
    <div class="pager-header">
        <div class="container">
            <div class="page-content">
                <h2>About Us</h2>
                <p> </p>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">About Us</li>
                </ol>
            </div>
        </div>
    </div>
    <section class="about-section bg-grey bd-bottom padding">
        <div class="container">
            <div class="row about-wrap">
                <div class="col-md-6 xs-padding">
                    <div class="about-image">
                        <img src="img/about.jpg" alt="about image">
                    </div>
                </div>
                <div class="col-md-6 xs-padding">
                    <div class="about-content">
                        <h2></h2>
                        <p>The trust is basically a non-profit organization with an intension to serve the public. The
                            aims as outlined in it’s constitution are predefined and to improve the status in the
                            treatment and management of arthritis


                            The areas which the trust has committed to work:</p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="team-section bd-bottom padding">
        <div class="container">
            <div class="section-heading text-center mb-40">
                <h2>The areas which the trust has committed to work:</h2>
                <span class="heading-border"></span>
                <p></p>
            </div>
            <div class="team-wrapper row">
                <div class="col-lg-3 sm-padding">
                    <div class="team-wrap row">
                        <div class="col-md-3">
                            <div class="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 sm-padding">
                    <div class="team-content">
                        <h3>To organize camps in less privileged areas.</h3>
                        <ul class="check-list">
                            <li><i class="fa fa-check"></i>To recognize and identify patients with arthritis at the
                                earliest and wherever possible to treat these patients.</li>
                            <li><i class="fa fa-check"></i>Patients needing further management are referred to
                                appropriate institutions.</li>
                            <li><i class="fa fa-check"></i>To identify the patients needing physical rehabilitation and
                                guide them.</li>
                            <li><i class="fa fa-check"></i>To understand both the privilege of the disease in the
                                community and their input on society</li>
                        </ul>
                    </div>
                    <div class="team-content">
                        <h3>To publish educational material to primary care proffesionals and to patients.</h3>
                        <ul class="check-list">
                            <li><i class="fa fa-check"></i>A bi-monthly periodical Arthritis New is circulated free of
                                cost to health professionals containing articles from experts in field of arthritis and
                                immunology.</li>
                            <li><i class="fa fa-check"></i>Support in the form of grant-in-aid for publications, which
                                disseminate information regarding the disease-arthritis.</li>
                        </ul>
                    </div>

                    <div class="team-content">
                        <h3>Promote Research activity in the field of arithritis</h3>
                        <ul class="check-list">
                            <li><i class="fa fa-check"></i>The trust has setup a fund that can support and develop
                                research activity in the field of arthritis and immunology. The fund will be set apart
                                each year to develop facility in any of the identified institute. A state of art
                                facility for research in the field of immunology and arthritis.</li>
                            <li><i class="fa fa-check"></i>Rehabilitation and vocational training for individuals
                                suffering from arthritis in Indian scenario is not adequate. This is mainly a
                                socio-economical issue and is the major area of thrust for the trust to operate upon.
                            </li>
                        </ul>
                    </div>
                    <div class="team-content">
                        <h3>Counting Medical Education Programme and training fellowship for clinical and paraclinical
                            professional</h3>
                        <ul class="check-list">
                            <li><i class="fa fa-check"></i> Organization of education lectures for the public, regarding
                                the various aspects of health and specifically about arthritis</li>
                            <li><i class="fa fa-check"></i>There is a proposal to encourage the young physician to go
                                for rheumatology and immunology training in the center of excellence in India. The
                                trainee will be financially supported to carry out the course. This is with an intention
                                to increase the number of personnel required to care for the growing number of patients
                                with arthritis</li>
                            <li><i class="fa fa-check"></i> In this scheme which we are proposing to start in due course
                                of time, training of social and psychosocial counsellors will be taken up. They will be
                                trained for both domiciliary and hospital based counselling of the patients.</li>
                        </ul>
                    </div>
                    <div class="team-content">
                        <h3>Direct and Indirect patient care for the needy and non-affordable</h3>
                        <ul class="check-list">
                            <li><i class="fa fa-check"></i> The patients who are financially deprived and cannot access
                                the best care for arthritis and immunological disorder are supported through a separate
                            fund earmarked for such activities.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include ('layout/footer.php'); ?>
    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/tether.min.js"></script>
    <script src="js/vendor/imagesloaded.pkgd.min.js"></script>
    <script src="js/vendor/owl.carousel.min.js"></script>
    <script src="js/vendor/jquery.isotope.v3.0.2.js"></script>
    <script src="js/vendor/smooth-scroll.min.js"></script>
    <script src="js/vendor/venobox.min.js"></script>
    <script src="js/vendor/jquery.ajaxchimp.min.js"></script>
    <script src="js/vendor/jquery.counterup.min.js"></script>
    <script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>
    <script src="js/vendor/jquery.slicknav.min.js"></script>
    <script src="js/vendor/jquery.nivo.slider.pack.js"></script>
    <script src="js/vendor/letteranimation.min.js"></script>
    <script src="js/vendor/wow.min.js"></script>
    <script src="js/contact.js"></script>
    <script src="js/main.js"></script>
</body>


</html>