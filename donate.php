<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include ('layout/header.php'); ?>
<div class="header-height"></div>
<div class="pager-header">
<div class="container">
<div class="page-content">
<h2>Donate</h2>
<p></p>
<ol class="breadcrumb">
<li class="breadcrumb-item active">Donate</li>
</ol>
</div>
</div>
</div>
<section class="contact-section" style="margin-top:-100px;">
<div class="container">
<div class="row contact-wrap">
For all the activities and the better community service of IARET, your co-operation and contributions are welcome. Your involvement in the form of financial contribution, Physical help or Moral support will go a long away in sustaining the ongoing activities and also enlarge their scope for better reach of the community. The trust has created a corpus of about Rs. 21,00,000/- as of December 2022 and proposes to use this interest of this amount annually for research support.

In the near future, with all of your support the trust envisages different corpus funds for different activities as follows:
<br>
1) Lupus Corpus Fund <br>
2) Research Corpus Fund <br>
3) Free Treatment Corpus Fund <br>
4) Publication/Education Fund
</div>


<div class="row contact-wrap">
<div class="col-md-6 ">
<div class="contact-info">
<h3>Our Bank Details for Cheque/NEFT/RTGS Transfer:</h3>
<p><b>Account Name : The Immunology & Arthritis Research & Education Trust (R)</b> <br>
Bank Name : Bank of Baroda  <br>
<b>Account Number : 89390100005824</b>  <br>
  ISFC CODE: BARB0VJMAWA ,<br>
Branch Name : Malleshwaram, Bangalore.<br>

</p>
<p></p>

</div>
</div>
<div class="col-md-6">
<div class="contact-form">


</div>
</div>
</div>
</div>
</section>
<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGm_weV-pxqGWuW567g78KhUd7n0p97RY"></script>

<script src="js/main.js"></script>
<script>

        (function($){ "use strict";

        /*=========================================================================
            Google Map Settings
        =========================================================================*/
                         
            google.maps.event.addDomListener(window, 'load', init);

            function init() {

                var mapOptions = {
                    zoom: 11,
                    center: new google.maps.LatLng(40.6700, -73.9400), 
                    scrollwheel: false,
                    navigationControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    draggable: false,
                    styles: [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]
                };

                var mapElement = document.getElementById('google_map');

                var map = new google.maps.Map(mapElement, mapOptions);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'Location!'
                });
            }

        })(jQuery);

        </script>
</body>
</html>