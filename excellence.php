<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>Excellence & Merit Awards</h2>
<small></small><br>
<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/excellence.jpg" alt="about image"> 
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2></h2>
<p>
In its annual programme of recognizing good scientific work related to Immunology & Rheumatology by the young scientists, the trust is presenting awards to best dissertations from medical postgraduates across the different medical colleges in India. In the past few years, the trust has given away 6 Awards of Excellence and another 6 awards of Merit with cash prize. </p>
<p>The trust sends notices and posters calling for submissions of dissertations to almost all medical universities, south and middle part of India and a few other states also. We are constantly increasing the sending of notices to cover all the medical universities in India. </p>
<p>The notices are sent early, and entries are sought from the fresh passed out medical postgraduates.</p>
<p>The dissertations copies received by mail or CD is sent to a review committee of Rheumatologists / Immunologists and evaluated on a structured evaluation scheme. On the recommendations of the committee, the awards are decided. The awards given out are; one Award of Excellence and one Award of Merit for the year as per the committees decision.</p>

<p>Till recently, the trust has given away 6 awards of excellence and 6 awards of merit over the last few year.</p>
<p><a href="iaretaward.php" class="btn btn-warning">IARET Awardees </a> &nbsp; <a href="Award-of-excellence.php" class="btn btn-warning">Award Of Excellence</a> &nbsp;<a href="poster.pdf" class="btn btn-warning">Notification for Awards </a></p>
</div>
</div>
</div>
</div>
</section>



<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>