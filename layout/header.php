<header id="header" class="header-section">
    <div class="top-header">
        <div class="container">
            <div class="top-content-wrap row">
                <div class="col-sm-8">
                    <ul class="left-info">
                        <li><a href="#"><i class="ti-email"></i><span class="__cf_email__"
                                    data-cfemail="a9e0c7cfc6e9f0c6dcdbedc6c4c8c0c787cac6c4">Email:
                                    iaret@chanrericr.com</span></a></li>
                        <li><a href="#"><i class="ti-mobile"></i>080 42516634/99</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 d-none d-md-block">
                    <ul class="right-info">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-header">
        <div class="container">
            <div class="bottom-content-wrap row">
                <div class="col-sm-2">
                    <div class="site-branding">
                        <a href="#"><img src="img/logo.png" alt="Brand" ></a>
                    </div>
                </div>
                <div class="col-sm-10 text-right">
                    <ul id="mainmenu" class="nav navbar-nav nav-menu">
                        <li class="active"> <a href="index.php">Home</a></li>
                        <li><a href="">Overview</a>
                            <ul>
                                <li><a href="mission.php">Mission & Vission</a></li>
                                <li><a href="overview.php">Overviews</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Achievements</a>
                            <ul>
                                <li><a href="excellence.php">Excellence and Merit</a></li>
                                <li><a href="achievements.php">Our Achievements</a></li>
                            </ul>
                        </li>
                        <li><a href="">Services</a>
                            <ul>
                                <li><a href="pateintsupport.php">Patient Support</a></li>
                                <li><a href="communitysup.php">Community Services</a></li>
                                <li><a href="eduactivity.php">Educational Activities</a></li>
                                <li><a href="research_support.php">Research Support</a></li>
                                <li><a href="annual-scientific-updates.php">Annual Scientific Updates</a></li>
                                <li><a href="publications.php">Publications</a></li>
                                <li><a href="lupus.php">Lupus Support Program</a></li>
                                <li><a href="drugbank.php">Drug Bank</a></li>
                            </ul>
                        </li>
                        <li><a href="">Know Us</a>
                            <ul>
                                <li><a href="gallery.php">Gallery</a></li>
                                <li><a href="team.php">Our Team</a></li>
                                <li><a href="achievements.php">Our Achievements</a></li>
                                <li><a href="about.php">About</a></li>
                                
                            </ul>
                        </li>
                        <li> <a href="blog.php">News & Events</a></li>
                        <li> <a href="contact.php">Contact</a></li>
                        <li><a href="donate.php">Donate Now</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>
