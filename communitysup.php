<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
.anchor a{
		border-radius: 36px;
background: #ffffff;
box-shadow:  18px 18px 36px #666666, 
             -18px -18px 36px #ffffff;
	}
	.anchor a:hover{
			border-radius: 36px;
background: #ffffff;
box-shadow: inset 18px 18px 36px #666666, 
            inset -18px -18px 36px #ffffff;
	}
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>Community Support</h2>

<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<!-- <img src="img/team/dm.png" alt="about image"> -->
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2>Organize camps in less privileged areas</h2>

<ul class="check-list">
<li><i class="fa fa-check"></i>a) To recognize and identify patients with arthritis at the earliest and advise them.  </li>
<li><i class="fa fa-check"></i>b) Patients needing further management are referred to appropriate institutions. </li>
<li><i class="fa fa-check"></i>c) To identify patients needing physical rehabilitation and guiding them.</li>
<li><i class="fa fa-check"></i>d) To understand both the prevalence of the disease in the community and their 
     impact on society</li>

</ul>

<h2>Services Extended: - </h2>
<p>Organization of Free Arthritis and Immune Disease camps <br>

More than 20 camps have been conducted in different rural and semi-urban area. These camps are conducted with the help of local organizations, wherein we expertise, medical personnel, needed laboratory support and disbursal of drugs as per prescriptions.
<br>
Patients identified with arthritis problem / suspected immunological diseases are directed to proper centers there, further proper diagnosis and management. 
 </p>
<p>
	Organization of Free School camps for general health check and joint diseases <br>

Initially the trust conducted number of school medical camps in nearby rural and semi -urban areas like Kaggalipura, etc.

</p>
<p>
Organization of educational programmes for public and primary care doctors <br>

Trust extends support for general public awareness programmes and primary care doctors education on request by social service organizations the trust will extend all the support for organizing these programmes. <br>
The Immunology & Arthritis Research & Education Trust was the partner in the recently conducted Walkathon for awareness of rheumatology and Walkathon on awareness of Lupus disease, etc.

</p>
<p>
	Printing and distribution of Patient awareness and education literature. <br>
Pamphlets describing patient support clinics, etc. and patient awareness booklets on particular diseases in regional language are also printed. 

</p>
</div>
</div>
</div>
</div>
</section>
<section>
	<div class="container">
		<div class="row"> 
			<div class="col-md-6 anchor" ><a href="" class="btn btn-block btn-warning">pamphlet</a></div>
			<div class="col-md-6 anchor" ><a href="" class="btn btn-block btn-warning">Booklet</a></div>
		</div>
	</div>
</section>
<br>

<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>