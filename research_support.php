<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>RESEARCH SUPPORT</h2>
<small>Promote research activity in the field of Immunology & Arthritis</small><br>
<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/research.jpg" alt="about image">
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">

<p>
	The IARET has the main objective of furthering the science of Immunology & Arthritis apart from the services to the patient and the public. 
  </p>
<p>Among the different objectives of the trust, Research Support is an important scientific support activity.</p>
<p>As a token of our endeavour to help the development of medical science and to give a needed primary assistance to the young medical graduates involved in research in Medical Colleges, the trust has the following initiatives: </p>
<h3>Types of Research Support: -</h3>
<ul class="check-list">
<li><h3>a) Research Grant</h3> Financial assistance to the tune of Rs. 25,000/- to one medical graduate in a year, pursuing any medical research in a medical college and finds it difficult to proceed due to financial crunch.
</li>
<li><h3>b) Research Publication Assistance </h3>Financial / Publication assistance extended for completion and publication of a 
research work in medical scientific area by a medical graduate in a medical 
college. This can be a grant of up to Rs. 20,000/- for one person in a year.
</li>
<li><h3>c) Fellowship Student Assistance (Fellowship in Immunology & Rheumatology)</h3>In an endeavour to assist the fellowship students in CRICR to complete their research as a part of their fellowship training programme, the trust supports them for expenses related to their research. </li>
</ul>

<a href="application.php" class="btn btn-warning">Click Here To Apply</a>
</div>
</div>
</div>
</div>
</section>



<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>