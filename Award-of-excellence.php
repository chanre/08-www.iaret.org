<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	th{
		text-align:center;
		background:#f8b864;
		line-height:35px;
		color:black;
		font-size:14px;
	}
	td{
		padding:10px;
	}
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">

<div class="container">
	<div class="section-heading text-center mb-40">
	<br><br>
<h2>AWARD OF EXCELLENCE</h2>
<small></small><br>
<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">

<div class="col-md-12 xs-padding">
<div class="about-content">
<table border="2">
<tr>
<th>Year</th>
<th>Name</th>
<th>Photo</th>
<th>Dissertation </th>
<th>Photo</th>
</tr>
<tr>
<td>2019</td>
<td>Dr. M. Ravi Teja Raidu
(Rajiv Gandhi University of Health Sciences, Karnataka, Bangalore)

</td>
<td>Photo</td>
<td>Diagnosis and assessment of the Severity of Systemic Lupus Erythematosus using SLICC criteria</td>
<td>Award Photo</td>
</tr>


<tr>
<td>2017</td>
<td>Dr. Harikrishnan. K
(Amala Institute of Medical Sciences, Thrissur)
</td>
<td>Photo</td>
<td>
A Study On The Prevalence Of Musculoskeletal Complications In Type 2 Diabetes Mellitus Patients And Find Its Determining Factors
</td>
<td>Award Photo</td>
</tr>

<tr>
<td>2016</td>
<td>Dr. B. Raga Deepthi
(Sri Venkateswara Institute of Medical Sciences, Tirupati)

</td>
<td>Photo</td>
<td>
Comparision of DAS28, CDAI , HAQ-DI and Rapid3
As tools to assess disease activity
In patients with rheumatoid arthritis
</td>
<td>Photo</td>
</tr>

<tr>
<td>2015</td>
<td>Dr. Suneetha Pulluru
(Sri Venkateswara Institute of Medical Sciences, Tirupati)
</td>
<td>Photo</td>
<td>Comparision of DAS28, CDAI , HAQ-DI and Rapid3
As tools to assess disease activity
In patients with rheumatoid arthritis
</td>
<td>Award Photo</td>
</tr>

<tr>
<td>2014</td>
<td>Dr. Sindhu Cugati
(Rajiv Gandhi University of Health Sciences, Karnataka)

</td>
<td>Photo</td>
<td>Immune Responses To Epstein-Barr Virus In Individuals With Rheumatoid Arthritis</td>
<td>Award Photo</td>
</tr>

<tr>
<td>2013</td>
<td>Dr. G. Sivaram Naik
(Sri Venkateswara Institute of Medical Sciences (SVIMS), Tirupati)
</td>
<td>Photo</td>
<td>Prevalence of thyroid disorders and
Metabolic syndrome in adult patients
With rheumatoid arthritis
</td>
<td>Award Photo</td>
</tr>

<tr>
<td>2011</td>
<td>Dr. Rajesh. S
(Rajiv Gandhi University of Health Sciences)
</td>
<td>Photo</td>
<td>Study of prevalence and pattern of Peripheral Neuropathy in Rheumatoid Arthritis
</td>
<td>Award Photo</td>
</tr>


</table>
</div>
</div>
</div>
</div>



</section>



<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>