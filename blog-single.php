<?php 
session_start();
include('layout/config.php');
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="DynamicLayers">

    <title>Blog Post</title>

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

    <!-- Font Awesome Icons CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Themify Icons CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- Elegant Font Icons CSS -->
    <link rel="stylesheet" href="css/elegant-font-icons.css">
    <!-- Elegant Line Icons CSS -->
    <link rel="stylesheet" href="css/elegant-line-icons.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Venobox CSS -->
    <link rel="stylesheet" href="css/venobox/venobox.css">
    <!-- OWL-Carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <!-- Slick Nav CSS -->
    <link rel="stylesheet" href="css/slicknav.min.css">
    <!-- Css Animation CSS -->
    <link rel="stylesheet" href="css/css-animation.min.css">
    <!-- Nivo Slider CSS -->
    <link rel="stylesheet" href="css/nivo-slider.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
    <!-- <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div> -->
    <!-- Preloader -->
    <?php include('layout/header.php');?>
    <div class="header-height"></div>
    <section class="blog-section bg-grey padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 sm-padding">
                    <?php
                        $pid=intval($_GET['nid']);
                        $currenturl="http://".$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];;
                        $query=mysqli_query($con,"select tblposts.PostTitle as posttitle,tblposts.PostImage,tblcategory.CategoryName as category,tblcategory.id as cid,tblsubcategory.Subcategory as subcategory,tblposts.PostDetails as postdetails,tblposts.PostingDate as postingdate,tblposts.PostUrl as url,tblposts.postedBy,tblposts.lastUpdatedBy,tblposts.UpdationDate from tblposts left join tblcategory on tblcategory.id=tblposts.CategoryId left join  tblsubcategory on  tblsubcategory.SubCategoryId=tblposts.SubCategoryId where tblposts.id='$pid'");
                        while ($row=mysqli_fetch_array($query)) {
                        ?>
                    <div class="blog-items single-post row">
                        <img src="admin/postimages/<?php echo htmlentities($row['PostImage']);?>" alt="blog post">
                        <h2><?php echo htmlentities($row['posttitle']);?></h2>
                        <div class="meta-info">
                            <span>
                                <i class="ti-user"></i> Posted By <a
                                    href="#"><?php echo htmlentities($row['postedBy']);?> </a>
                            </span>
                            <span>
                                <i class="ti-bookmark"></i> Tagged As <a href="#">IARET</a>
                            </span>
                        </div><!-- Meta Info -->
                        <?php 
              $pt=$row['postdetails'];
              echo  (substr($pt,0));?>

                        <div class="share-wrap">
                            <h4>Share This Article</h4>
                            <ul class="share-icon">
                                <li><a href="#"><i class="ti-facebook"></i> Facebook</a></li>
                                <li><a href="#"><i class="ti-twitter"></i> Twitter</a></li>
                                <li><a href="#"><i class="ti-google"></i> Google+</a></li>
                                <li><a href="#"><i class="ti-instagram"></i> Instagram</a></li>
                                <li><a href="#"><i class="ti-linkedin"></i> Linkedin</a></li>
                            </ul>
                        </div>
                        <!-- Share Wrap -->
                        <?php } ?>

                    </div>
                </div><!-- Blog Posts -->
                <div class="col-lg-3 sm-padding">
                    <div class="sidebar-wrap">

                        <!-- Categories -->
                        <div class="sidebar-widget mb-50">
                            <h4>Recent Posts</h4>
                            <ul class="recent-posts">
                                <?php 
                                $query1=mysqli_query($con,"select tblposts.id as pid,tblposts.PostTitle as posttitle,tblposts.PostImage,tblcategory.CategoryName as category,tblcategory.id as cid,tblsubcategory.Subcategory as subcategory,tblposts.PostDetails as postdetails,tblposts.PostingDate as postingdate,tblposts.PostUrl as url from tblposts left join tblcategory on tblcategory.id=tblposts.CategoryId left join  tblsubcategory on  tblsubcategory.SubCategoryId=tblposts.SubCategoryId where tblposts.Is_Active=1 order by tblposts.id desc  LIMIT 5");
                                while ($row1=mysqli_fetch_array($query1)) {
                                
                                ?>
                                <li>
                                    <img src="admin/postimages/<?php echo htmlentities($row1['PostImage']);?>"
                                        alt="blog post">
                                    <div>
                                        <h4><a
                                                href="blog-single.php?nid=<?php echo htmlentities($row1['pid'])?>"><?php echo htmlentities($row1['posttitle']);?></a>
                                        </h4>
                                        <span class="date"><i class="fa fa-clock-o"></i>
                                            <?php echo htmlentities($row1['postingdate']);?></span>
                                    </div>
                                </li>
                                <?php } ?>

                            </ul>
                        </div><!-- Recent Posts -->
                        <div class="sidebar-widget mb-50">
                            <!-- <h4>Tags</h4>
                            <ul class="tags">
                                <li><a href="#">Audio</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Design</a></li>
                                <li><a href="#">Magazine</a></li>
                                <li><a href="#">Gallery</a></li>
                                <li><a href="#">Creative</a></li>
                                <li><a href="#">Post</a></li>
                                <li><a href="#">Quotes</a></li>
                            </ul> -->
                        </div><!-- Tag -->
                    </div><!-- /Sidebar Wrapper -->
                </div>
            </div>
        </div>
    </section>
    <?php include('layout/footer.php');?>
    <!-- /Footer Section -->
    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>
    <!-- jQuery Lib -->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/vendor/bootstrap.min.js"></script>
    <!-- Tether JS -->
    <script src="js/vendor/tether.min.js"></script>
    <!-- Imagesloaded JS -->
    <script src="js/vendor/imagesloaded.pkgd.min.js"></script>
    <!-- OWL-Carousel JS -->
    <script src="js/vendor/owl.carousel.min.js"></script>
    <!-- isotope JS -->
    <script src="js/vendor/jquery.isotope.v3.0.2.js"></script>
    <!-- Smooth Scroll JS -->
    <script src="js/vendor/smooth-scroll.min.js"></script>
    <!-- venobox JS -->
    <script src="js/vendor/venobox.min.js"></script>
    <!-- ajaxchimp JS -->
    <script src="js/vendor/jquery.ajaxchimp.min.js"></script>
    <!-- Counterup JS -->
    <script src="js/vendor/jquery.counterup.min.js"></script>
    <!-- waypoints js -->
    <script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>
    <!-- Slick Nav JS -->
    <script src="js/vendor/jquery.slicknav.min.js"></script>
    <!-- Nivo Slider JS -->
    <script src="js/vendor/jquery.nivo.slider.pack.js"></script>
    <!-- Letter Animation JS -->
    <script src="js/vendor/letteranimation.min.js"></script>
    <!-- Wow JS -->
    <script src="js/vendor/wow.min.js"></script>
    <!-- Contact JS -->
    <script src="js/contact.js"></script>
    <!-- Main JS -->
    <script src="js/main.js"></script>
</body>
</html>