<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>Lupus Support Programme</h2>
<small></small><br>
<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="images/lupus.jpg" alt="about image"> 
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<!-- <h2>LUPUS SUPPORT PROGRAMME</h2> -->
    <p>
    The Immunology & Arthritis Research & Education Trust ® (IARET)  is created with the clear idea of catering to the needs of non-affordable in getting specialist care in Immunological and Rheumatological diseases, to promote research in  Immunology and Rheumatology, to undertake awareness programmes both for the primary physicians and general public as also to conduct free camps and clinics to identify new cases and help them get proper therapy. (Please refer to our website www.iaret.org for further details).
    </p>
  
<p>
The trust has the vision of being empathetic to the community by service and join many hands together in serving the humanity, in addition to other visions. Under this vision, our mission is to support non-affordable patients with proper diagnosis and free or subsidized treatment for the management of their immunological and rheumatological diseases such as Arthritis, SLE, etc.  
</p>
<p>Under this broad understanding, the trust is envisaging the creation of support funds for different specific activities of the ongoing trust efforts.</p>
<p>One such support activity is the creation of a fund and carrying forward concentrated activity in the area of Systemic Lupus Erythematosus (Lupus) disease</p>


<h3>Lupus Support Programme</h3>
<p>Systemic Lupus Erythematosus (SLE) is a chronic autoimmune disease, causing inflammation and tissue damage in the affected organs, including the joints, skin, brain, lungs, kidneys and blood vessels. It can range from mild to life-threatening with only medical interventions and lifestyle changes available to control disease, as there is no definitive cure.</p>
<p>SLE management lasts for a lifetime and is often expensive. In recent decades, SLE treatment has moved from the use of Hydroxychloroquine, Systemic Glucocorticoids and conventional Immuno-suppressive drugs to next generation monoclonal antibody such as Rituximab and Belimumab that are tagged with high costs ranging from Rs. 50,000 to several lakhs.  The insurance and third-party policies do not cover the costs for SLE treatment leaving the patients in a lurch, not only making it difficult to cope up with the disease but also leaving them in a financial turmoil. As a matter of fact, neither the State nor Central Governments have a provision / scheme to pitch in for SLE healthcare. </p>
<p>We, at The Immunology & Arthritis Research & Education Trust ® (IARET)  would like to help overcome the affordability issue and make healthcare for SLE more accessible to the non-affordable. We envisage initiating a Lupus Support Fund specifically earmarked to serve the non-affordable with SLE. The funds collected for the same will serve as a conduit for a good social cause with an emphasis on healthcare to everyone. We plan to expand this initiative for the patients from the In-house ChanRe Rheumatology & Immunology Center & Research (CRICR) and general healthcare / specifically arthritis medical camps conducted by the trusts but also to patients identified in other centers across the city Bengaluru, Karnataka State and Later elsewhere also. We believe that this initiative will help improve the quality of life of SLE patients to lead a comfortable life with fewer complications.</p>
<h3>The Disease SLE  (Overview) </h3>
<p>In India SLE prevalence has been reported to be 3.2 per 100,000 populations. </p>
<p>SLE is characterized with variety of symptoms that include fatigue, skin rashes, fever, pain and swelling in the joints and more severe symptoms related to lung involvement, heart involvement, kidney involvement, seizures, psychosis and immunological abnormalities. </p>
<p>While early diagnosis and effective treatment can help reduce the damaging effects of SLE and improve the chance to have better function and quality of life, the diagnosis itself is very challenging. The symptoms can overlap with other conditions.</p>
<p>The process of diagnosis depends on taking a proper history, identifying basic and specific features and could include investigation of a few blood parameters, blood cell count, Anti-Nuclear Antibody (ANA) test, etc. The major issue is the high cost involved in the diagnosis and the control of the disease with medical management over a semi-long to long period. The lack of healthcare affordability from the economically weaker section of the Indian population poses an entirely difficult challenge altogether.</p>
<h3>Primary Objectives of the SLE Support Programme</h3>
<ol>
<li>Facilitating proper diagnosis and medical management including drug disbursal to non-affordable patients. (Non-affordability status evaluated by standardized checklist maintained by the trust.)</li>

<li>Supplementary support for follow-ups, Day-care and hospitalization in case of need. </li>

<li>Patients needing further management are referred to appropriate institutions.</li>

<li>To counsel patients when in mental stress and to improve their ability to adopting to the challenges of the disease. </li>

<li>To create Free SLE Support Clinic for identified patients followup requirements.</li>

<li>To improve the quality of life of patient, care givers awareness through Group Talks and Educational Programmes. </li>

<li>Support for any disability in the patient due to SLE. </li>

<li>Spread awareness through social media and campaigns, etc. </li>

<li>Facilitating direct donor support to individual patient care.</li>
</ol>
<h3>Secondary Objectives of the SLE Support Programme</h3>
<p>
To find out and support research activities across India related to SLE disease, its diagnostic improvements, new ideas in medical management and life-style management of the patients. 
</p>
<h3>The efforts of the trust as of now</h3>
<p>The IARET has a base at ChanRe Rheumatology & Immunology Center & Research. The counsellor from the trust Identifies non-affordable patients visiting the clinic for various immunological and rheumatological diseases. He counsels them, help them by way of recommending free consultation, free / subsidized investigation through the trust on a standardized evaluation plan. The trust also extends medical benefit by way of drug distribution through the trust drug bank. </p>
<p>This service of helping the needy patients extends even to their repeated regular followups required for the proper treatment of the Lupus and other diseases</p>
<p>As it is medical known SLE patients require repeated visits to the consultant  and also expensive treatment protocol for the inevitable flares happening in the disease. This needs repeated outflow of expense from the patients for maintaining their difficult lifestyle also.</p>
<h3>Plan for creating a Lupus support fund</h3>
<p>The trust is finding it difficult to manage these non-affordable patients for their repeated need for support towards their medical management. Hence the trust is planning to create a perpetual support fund by the contribution from the General Public, CSR funding from industries and other donations form philanthropic organizations, etc.</p>
<p>Initially, the part of the accrued fund will be used to directly support the needy patients and the remaining portion of the fund will be used to create a long-term support corpus fund, the interest from the same to be used for the expansion of this programme. This is the basic plan we are envisaging.</p>
<h3>How can you get involved in the service towards SLE patients</h3>
<p>
    <ol>
        <li>By contributing cash / drugs to the trust.</li>
        <li>You can support an individual patient for all their / part needs. The trust will identify the patient and make the needed contacts and modalities for you to interact and extend your help to them through the trust portal.</li>
        <li>CSR funding from industries for creation of support fund and also specific activities on a joint basis through MoU</li>
        <li>Specific patient awareness and education programmes can be funded by those who are interested. </li>
        <li>Any other method of involvement for the extension of this SLE support activity.</li>
    </ol>
</p>
<h3>Need for creation of a separate Lupus Support Fund</h3>
<p>With the experience of this trust associated with CRICR, we are coming across around 5 to 6 patients of SLE in the category of non-affordability every year as per the OPD and in-patients attendance in the institute. Approximate initial investigation and primary treatment cost comes to around 10,000 to 15,000 Rupees for patient. Added to this, there is a need for repeated followups, including investigating the inevitable flares that can occur in this disease. </p>
<p>In serious patients with flare, there will be need for admission in the hospital and expensive investigation and treatment by way of infusion and other modalities. This will cost at the minimum around 50,000 Rupees per episode.</p>
<p>Keeping all these in view, we need to have a fund of around minimum of Rs. 5,00,000/- expendable per year in this SLE support alone. The trust with its vision of expanding this programme to other non-affordable patients from other institutions, we will need a larger fund of around Rs. 20,00,000/- or so. </p>
<p>With this scenario of expenses for this activity, we need to depend on general public, CSR support from industries and donations from philanthropic organizations to create a large corpus fund which can sustain this activity for a long period and expand & enlarge this in coming years.
We request your kind support.
</p>
<h3>CONTACT</h3>
<p>The Immunology & Arthritis Research & Education Trust® (IARET) <br>
ChanRe Rheumatology & Immunology Center & Research <br>
#414/65, 20th Main, West of Chord Road, <br>
1st Block, Rajajinagar, Bengaluru - 560 010 <br>
Phone: - 080-42516635 <br>
Website: www.iaret.org


</p>
<p>Administrator - IARET: 9880023281 / 080-42516635</p>
<p>Counsellor / Social Worker - IARET: 9019028476 / 080-42516699</p>
</div>
</div>
</div>
</div>
</section>



<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>