<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="DynamicLayers">
    <title>GALLERY - IARET</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/elegant-font-icons.css">
    <link rel="stylesheet" href="css/elegant-line-icons.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/venobox/venobox.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/slicknav.min.css">
    <link rel="stylesheet" href="css/css-animation.min.css">
    <link rel="stylesheet" href="css/nivo-slider.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
    <div class="site-preloader-wrap">
        <div class="spinner"></div>
    </div>
    <?php include('layout/header.php');?>
    <div class="header-height"></div>

    <section class="gallery-section bg-grey bd-bottom padding">
        <div class="container">
            <div class="row">
                <ul class="gallery-filter align-center mb-30">
                    <li class="active" data-filter="*">Gallery</li>
                    <!-- <li data-filter=".branding">Branding</li> -->
                    <!-- <li data-filter=".design">Design</li> -->
                    <!-- <li data-filter=".wordpress">Wordpress</li> -->
                    <!-- <li data-filter=".marketing">Marketing</li> -->
                </ul>
            </div>
            <div class="gallery-items row">
                <!-- <div class="col-lg-4 col-sm-6 single-item branding design">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/med1.jpg" class="card-img-top" alt="...">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 single-item branding design">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/med2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-4 col-sm-6 single-item branding design">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/gallery/1.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"> <b>Dr Chandrashekara S</b> Managing Trustee Has Handing over the
                                    cheque From samvada for Patient support in Covid-19 P endemic </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 single-item marketing wordpress">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/gallery/2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">IARE Trust organized Reproductive immunology Helth camp in
                                    association with Arogya bharathi and Chanre Rheumatology &immunology center &
                                    Research at Hosayalanadu ,Hiriyuru (T) on 31 July 2021 Awareness talk given by Dr.
                                    Chaitra Nirantara For Rural women's</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 single-item wordpress design branding">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/gallery/3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">IARE Trust organized Reproductive immunology Helth camp in
                                    association with Arogya bharathi and Chanre Rheumatology &immunology center &
                                    Research at Hosayalanadu ,Hiriyuru (T) on 31 July 2021 Awareness talk given by Dr.
                                    Chaitra Nirantara For Rural women's</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 single-item design branding wordpress">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/gallery/4.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">The IARE trust organized Arthritis and Reproductive immunology
                                    Health camp @ Jalahalli village Bangalore,association with Parva health care and
                                    ChanRe RICR On 08 August 2021 Dr.Praveen Consultant rheumatologist has Treated 55
                                    Patients To their camp And Distributed Orthotics and medications</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 single-item branding marketing">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/gallery/5.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">The IARE trust organized Arthritis and Reproductive immunology
                                    Health camp @ Jalahalli village Bangalore,association with Parva health care and
                                    ChanRe RICR On 08 August 2021 Dr.Praveen Consultant rheumatologist has Treated 55
                                    Patients To their camp And Distributed Orthotics and medications</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 single-item marketing design">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/gallery/6.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">world lupus day celebrate and awareness camp on association with
                                    CRICR</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 single-item marketing design">
                    <div class="gallery-wrap">
                        <div class="card">
                            <img src="img/gallery/7.jpeg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text">IARE Trust Organized Awareness Programme Talk on Rheumatoid
                                    Arthritis Association with CRICR on 27 March 2021 Dr Shruthi give a Demonstration
                                    talk to the Patients and care givers </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <?php include('layout/footer.php'); ?>
    <a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/tether.min.js"></script>
    <script src="js/vendor/imagesloaded.pkgd.min.js"></script>
    <script src="js/vendor/owl.carousel.min.js"></script>
    <script src="js/vendor/jquery.isotope.v3.0.2.js"></script>
    <script src="js/vendor/smooth-scroll.min.js"></script>
    <script src="js/vendor/venobox.min.js"></script>
    <script src="js/vendor/jquery.ajaxchimp.min.js"></script>
    <script src="js/vendor/jquery.counterup.min.js"></script>
    <script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>
    <script src="js/vendor/jquery.slicknav.min.js"></script>
    <script src="js/vendor/jquery.nivo.slider.pack.js"></script>
    <script src="js/vendor/letteranimation.min.js"></script>
    <script src="js/vendor/wow.min.js"></script>
    <script src="js/contact.js"></script>
    <script src="js/main.js"></script>
</body>


</html>