<!doctype html>

<html class="no-js" lang="en"> 


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="DynamicLayers">
<title>Immunology & arithritis Research & Eduaction Trust</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/themify-icons.css">

<link rel="stylesheet" href="css/elegant-font-icons.css">

<link rel="stylesheet" href="css/elegant-line-icons.css">

<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/venobox/venobox.css">

<link rel="stylesheet" href="css/owl.carousel.css">

<link rel="stylesheet" href="css/slicknav.min.css">

<link rel="stylesheet" href="css/css-animation.min.css">

<link rel="stylesheet" href="css/nivo-slider.css">

<link rel="stylesheet" href="css/main.css">

<link rel="stylesheet" href="css/responsive.css">
<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<style type="text/css">
	
</style>
</head>
<body>

<div class="site-preloader-wrap">
<div class="spinner"></div>
</div>
<?php include('layout/header.php'); ?>
<div class="header-height"></div>

<section class="about-section bd-bottom padding">
<div class="container">
	<div class="section-heading text-center mb-40">
<h2>Our Achievements</h2>
<small></small><br>
<span class="heading-border"></span>
</div>
<br>
<div class="row about-wrap">
	
<div class="col-md-4 xs-padding">
<div class="about-image">
<img src="img/achievement.jpg" alt="about image"> 
</div>
</div>
<div class="col-md-8 xs-padding">
<div class="about-content">
<h2></h2>
<p>
	Spanning the last two decades, the trust has grown in its structure, activities, and stability also. It is time for us now to enlarge our activities to reach the community in larger measures in the next decade. To understand how we started and developed over the period, we can see the following brief notes on the different milestones crossed by the trust:
  </p>
  <h3>The initial steps</h3>
<p>
	<ul class="check-list">
<li><i class="fa fa-check"></i>1) The trust supported publication of News Letters to primary physicians – Arthritis News and further extended the support for the publication of patient education booklets in local language.  </li>
<li><i class="fa fa-check"></i>2) In the initial few years, the trust conducted many medical camps in rural and semi-urban areas to increase awareness regarding arthritis and also to identify patients in the early stage of disease. </li>
<li><i class="fa fa-check"></i>3)	Non-affordable patient support started with the active support of CRICR and CDL.</li>
</ul>
</p>
<h3>Steady Developments</h3>
<p>1) From the year 2003, till date the trust has led the organizing of the Annual CME programme ChanRe Update with the support of CRICR and CDL. We are happy to inform that continuously over the years, this annual programme has seen 17 editions and over the period has acquired a niche in the scientific arena of Immunology & Arthritis attracting many practicing physicians and post graduate students. In this juncture, we thank all the faculties from many parts of India who participated and enriched the deliberations. </p>
<p>2) Continuation and enlargement of patient support system by standardized evaluation to identify the needy non-affordable and structuring the types of benefits that are extended through the trust and CRICR & CDL. A few examples are  Free Consultations, Free/Subsidized Investigations, Subsidized hospitalization charges and Free Drug Dispensing, etc.  </p>

<p>As per the request of some supporters and donors, we have evolved a system of direct one to one support to patients from individuals or organizations so as to help the non-affordable patients to overcome the difficulties of expenses related to their needed treatment, in full or in part including any required followup medical management. </p>
<p>More than 100 patients are already supported from the trust. Many of these patients 
require regular followup visits which is also covered under this scheme. The trust has 
extended support for a total of 1200 followup visits for all of these patients including 
disbursal of available drugs from the trust drug bank. 
</p>
<h3>Diversification of Activities</h3>
<p>Conduct of different educational programmes for primary physicians / professionals in the form of many annual / bi-annual and occasional scientific programmes by the trust or with the support of other organizations. These activities are already listed in other sections. (Make this sentence as link to Educational Activities page – Under the tab Services and Activities ) .</p>
<h3>Research Support</h3>
<p>As listed elsewhere,  trust is contributing on different levels for the development of proper knowledge in the field of Arthritis and Immunology. This has been done through different ways, continued, and further enlarged over the years. </p>
<h3>Recognition of talent</h3>
<p>To find out the nascent talent in the postgraduates in medicine and to induce them to take up studies in the field of Arthritis and Immunology, the trust is doing an annual programme of “IARET Award of Excellence for Postgraduate Research in Rheumatology & Immunology”.</p>
</div>
</div>
</div>
</div>
</section>



<?php include ('layout/footer.php'); ?>
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/vendor/jquery-1.12.4.min.js"></script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/vendor/tether.min.js"></script>

<script src="js/vendor/imagesloaded.pkgd.min.js"></script>

<script src="js/vendor/owl.carousel.min.js"></script>

<script src="js/vendor/jquery.isotope.v3.0.2.js"></script>

<script src="js/vendor/smooth-scroll.min.js"></script>

<script src="js/vendor/venobox.min.js"></script>

<script src="js/vendor/jquery.ajaxchimp.min.js"></script>

<script src="js/vendor/jquery.counterup.min.js"></script>

<script src="js/vendor/jquery.waypoints.v2.0.3.min.js"></script>

<script src="js/vendor/jquery.slicknav.min.js"></script>

<script src="js/vendor/jquery.nivo.slider.pack.js"></script>

<script src="js/vendor/letteranimation.min.js"></script>

<script src="js/vendor/wow.min.js"></script>

<script src="js/contact.js"></script>

<script src="js/main.js"></script>
</body>


</html>